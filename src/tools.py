import sys
import pygame
import math

from shape import Shape

class Tools:
    Palette = 0
    Line = 1
    Circle = 2
    Rectangle = 3

    def __init__ (self, canvas_size):
        ## canvas
        self._canvas_size = canvas_size

        ## current tool setup
        self._active = Tools.Line
        self._previous = Tools.Palette
        self._pos = (0, 0)

    def get_active_tool (self):
        return self._active

    def switch_tool (self, key):
        self._previous = self._active
        
        if (key == pygame.K_p):
            self._active = Tools.Palette
        elif (key == pygame.K_l):
            self._active = Tools.Line
        elif (key == pygame.K_c):
            self._active = Tools.Circle
        elif (key == pygame.K_r):
            self._active = Tools.Rectangle
        else:
            sys.stderr.write('[ERROR] unknown tool for ' + chr(key) + '\n')

    def switch_back (self):
        self._active = self._previous

    def set_position (self, pos):
        self._pos = pos

    def make_shape (self, color, pos):
        surface = pygame.Surface(self._canvas_size, pygame.SRCALPHA, 32)

        if self._active == 1:
            pygame.draw.line(surface,color,self._pos,pos)
        elif self._active == 2:
            pygame.draw.circle(surface,color,self._pos,int(math.sqrt((pos[0]-self._pos[0])**2 + (pos[1]-self._pos[1])**2)))
        elif self._active == 3:
            # Les IF sont utilisés pour la compréhension du code.
            # HAUT DROITE
            if self._pos[1] > pos [1]  and self._pos[0] < pos[0]:
                pygame.draw.rect(surface,color,(self._pos[0],self._pos[1],pos[0]-self._pos[0],pos[1]-self._pos[1]))
            # HAUT GAUCHE
            if self._pos[1] > pos [1]  and self._pos[0] > pos[0]:
                pygame.draw.rect(surface,color,(self._pos[0],self._pos[1],pos[0]-self._pos[0],pos[1]-self._pos[1]))
            # BAS GAUCHE
            if self._pos[1] < pos [1]  and self._pos[0] > pos[0]:
                pygame.draw.rect(surface,color,(self._pos[0],self._pos[1],pos[0]-self._pos[0],pos[1]-self._pos[1]))
            # BAS DROITE
            if self._pos[1] < pos [1]  and self._pos[0] < pos[0]:
                pygame.draw.rect(surface,color,(self._pos[0],self._pos[1],abs(self._pos[0]-pos[0]),abs(self._pos[1]-pos[1])))

        return Shape(surface)
