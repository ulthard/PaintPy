import pygame

from palette import *
from tools import *

class MiniPaint:

    def __init__ (self):
        ## initialize pygame
        pygame.init()

        ## open new window
        self._window = pygame.display.set_mode((600, 420), pygame.DOUBLEBUF)
        pygame.display.set_caption('MiniPaint')

        ## create canvas
        self._canvas = pygame.Surface((600, 420), pygame.SRCALPHA, 32)

        ## initialize MiniPaint
        self._bgcolor = pygame.Color(255, 255, 255, 255) # white
        self._shapes = pygame.sprite.OrderedUpdates()
        self._palette = Palette(self._canvas.get_size())
        self._tools = Tools(self._canvas.get_size())

    def run (self):
        done = False
        tmp = None
        while not done:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    done = True
                
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        self._tools.set_position(event.pos)
                
                if event.type == pygame.MOUSEBUTTONUP:
                    if event.button == 1:
                        self._shapes.add(self._tools.make_shape(self._palette.get_color(),event.pos))
                
                if event.type == pygame.MOUSEMOTION:
                    if self._tools.get_active_tool() == Tools.Palette:
                        self._palette.update_color(event.pos)
                        if event.buttons[0] == 1:
                            self._palette.pick_color()
                    else :
                        if event.buttons[0] == 1:
                            tmp = self._tools.make_shape(self._palette.get_color(),event.pos)
                        if event.buttons[0] == 0:
                            tmp = None

                if event.type == pygame.KEYDOWN:
                    if event.mod & pygame.KMOD_CTRL:
                        if event.key == pygame.K_p:
                            self._tools.switch_tool(pygame.K_p)
                        elif event.key == pygame.K_l:
                            self._tools.switch_tool(pygame.K_l)
                        elif event.key == pygame.K_c:
                            self._tools.switch_tool(pygame.K_c)
                        elif event.key == pygame.K_r:
                            self._tools.switch_tool(pygame.K_r)
                        elif event.key == pygame.K_s:
                            pygame.image.save(self._canvas,"image.png")
                    if event.key == pygame.K_ESCAPE:
                        self._tools.switch_back()
            
            ## clear window
            self._canvas.fill(self._bgcolor)

            ## draw existing shapes
            self._shapes.draw(self._canvas)

            ## draw current shape
            if tmp != None:
                text = "L'outil séléctionné est : "
                if self._tools._active == 0 :
                    outil = "Palette"
                elif self._tools._active == 1:
                    outil = "Line"
                elif self._tools._active == 2:
                    outil = "Circle"
                else:
                    outil = "Rectangle"
                #print(text + outil)
                self._canvas.blit(tmp.image,tmp.rect)
            
            ## display color picker
            if(self._tools.get_active_tool() == Tools.Palette):
                self._canvas.blit(self._palette.get_color_picker(),(0,0))

            ## update display
            self._window.blit(self._canvas, (0, 0))
            pygame.display.flip()

        pygame.quit()

if __name__ == '__main__':
    mp = MiniPaint()
    mp.run()
